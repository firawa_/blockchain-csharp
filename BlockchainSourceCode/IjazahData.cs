﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlockchainSourceCode
{
    class IjazahData
    {
        public string nama { get; set; }
        public string tempatLahir { get; set; }
        public DateTime tanggalLahir { get; set; }
        public string intansiName { get; set; }
        public string noIjazah { get; set; }
        public DateTime tglIjazah { get; set; }
        public int thnIjazah { get; set; }

        public Byte[] scanFile { get; set; }
    }
}
