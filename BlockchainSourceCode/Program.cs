﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Data;
using Npgsql;
using System.ComponentModel;
using System.Data.SqlTypes;

namespace BlockchainSourceCode
{
    class Program
    {
        public static double fee = 10.0;

        public static List<Transaction> open_data = new List<Transaction>();

        public static List<Block> Blockchain = new List<Block>();

        public static string CreateHash(string block)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(block));

                StringBuilder builder = new StringBuilder();
                for (int subscript = 0; subscript < bytes.Length; subscript++)
                {
                    builder.Append(bytes[subscript].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static bool ValidProof(List<Transaction> input_data, string last_hash, int nonce)
        {
            string jsonstring = JsonConvert.SerializeObject(input_data);
            string exp = jsonstring + last_hash + Convert.ToString(nonce);
            string guess_hash = CreateHash(exp);
            return guess_hash.Substring(0, 2) == "30";
        }

        public static int PowNonce()
        {
            var last_block = Blockchain[Blockchain.Count - 1];
            string jsonstring = JsonConvert.SerializeObject(last_block);
            string last_hash = CreateHash(jsonstring);

            int nonces = 0;
            while (ValidProof(open_data, last_hash, nonces) == false)
            {
                nonces++;
            }
            return nonces;
        }

        public static void add_data(string pengirim, Certificates in_data)
        {
            RewardTransactions rewards = new RewardTransactions();

            rewards.Pengirim = "Bisnis Tekno Utama";
            rewards.Penerima = "Miners";
            rewards.fee = fee;

            var transaction = new Transaction
            {
                RewardTransactions = rewards,
                Pemilik = pengirim,
                Penyimpan = "Bisnis Tekno Utama",
                certificates = in_data
            };

            open_data.Add(transaction);
        }

        public static List<Block> MineBlock()
        {
            var last_block = Blockchain[Blockchain.Count - 1];
            string jsonstring = JsonConvert.SerializeObject(last_block);
            string hashed_block = CreateHash(jsonstring);
            int nonce = PowNonce();

            var block = new Block
            {
                Index = Blockchain.Count,
                Nonce = nonce,
                Waktu = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                PreviousHash = hashed_block,
                Transactions = open_data
            };

            Blockchain.Add(block);

            return Blockchain;
        }

        public static Byte[] GetArrayFromFile(string path)
        {
            Byte[] data = null;

            FileInfo fileInf = new FileInfo(path);
            long numBytes = fileInf.Length;

            FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read);

            BinaryReader bReader = new BinaryReader(fStream);

            data = bReader.ReadBytes((int)numBytes);
            return data;
        }

        public static Certificates get_data(Byte[] content, string courseName, int credits, string keywords, string majorName, string name, string strata, string organization, double score, int semester, DateTime testDate, string testName, DateTime timestamp, string type, int channel)
        {
            Certificates certificates = new Certificates();

            certificates.content = content;
            certificates.courseName = courseName;
            certificates.credits = credits;
            certificates.keywords = keywords;
            //certificates.mailaddr = mailaddr;
            certificates.majorName = majorName;
            certificates.name = name;
            certificates.strata = strata;
            certificates.organization = organization;
            certificates.score = score;
            certificates.semester = semester;
            certificates.testDate = testDate;
            certificates.testName = testName;
            certificates.timestamp = timestamp;
            certificates.type = type;
            certificates.channel = channel;

            return certificates;
        }
        /*public static IjazahData get_data(string nama, string tempatLahir, DateTime tanggalLahir, string instansi, string nomorijazah, DateTime tanggalijazah, int tahun, Byte[] content)
        {
            IjazahData ijazahdata = new IjazahData();

            ijazahdata.nama = nama;
            ijazahdata.tempatLahir = tempatLahir;
            ijazahdata.tanggalLahir = tanggalLahir.Date;
            ijazahdata.intansiName = instansi;
            ijazahdata.noIjazah = nomorijazah;
            ijazahdata.tglIjazah = tanggalijazah.Date;
            ijazahdata.thnIjazah = tahun;
            ijazahdata.scanFile = content;
            return ijazahdata;
        }*/

        public static List<Block> add_add_data(string mailaddr, Byte[] content, string coursename, int credits, string keywords, string majorname, string name, string strata, string organization, double score, int semester, DateTime testdate, string testname, DateTime timestamp, string type, int channel)
        {
            Certificates certificates = get_data(content, coursename, credits, keywords, majorname, name, strata, organization, score, semester, testdate, testname, timestamp, type, channel);
            add_data(mailaddr, certificates);
            //IjazahData ijazahData = get_data(nama, tempatlahir, tanggallahir, instansi, nomorijazah, tanggalijazah, tahun, content);
            //add_data(email, ijazahData);

            var AllBlock = MineBlock();
            return AllBlock;
        }
        static void Main(string[] args)
        {
            var genesis_block = new Block
            {
                Index = 0,
                Nonce = 30,
                Waktu = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                PreviousHash = "0000000000000000000000000000000000000000000000000000000000000000",
                Transactions = new List<Transaction>()
            };

            Blockchain.Add(genesis_block);

            bool x = true;

            while (x == true)
            {
                Console.WriteLine("Email: ");
                string Mailaddr = Console.ReadLine();

                Console.WriteLine("Path File: ");
                string path = Console.ReadLine();
                Byte[] Content = GetArrayFromFile(path);

                Console.WriteLine("Course Name");
                string CourseName = Console.ReadLine();

                Console.WriteLine("Credit");
                int Credits = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Keywords: ");
                string Keywords = Console.ReadLine();

                Console.WriteLine("MajorName: ");
                string MajorName = Console.ReadLine();

                Console.WriteLine("Name: ");
                string Name = Console.ReadLine();

                Console.WriteLine("Strata: ");
                string Strata = Console.ReadLine();

                Console.WriteLine("Organization: ");
                string Organization = Console.ReadLine();

                Console.WriteLine("Score");
                double Score = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Semester");
                int Semester = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("TestDate: ");
                DateTime TestDate = DateTime.Parse(Console.ReadLine()).Date;

                Console.WriteLine("TestName: ");
                string TestName = Console.ReadLine();

                Console.WriteLine("Timestamp: ");
                DateTime Timestamp = DateTime.Parse(Console.ReadLine());

                Console.WriteLine("Type: ");
                string Type = Console.ReadLine();

                Console.WriteLine("Channel: ");
                int Channel = Convert.ToInt32(Console.ReadLine());        

                List<Block> allblock = add_add_data(Mailaddr, Content, CourseName, Credits, Keywords, MajorName, Name, Strata, Organization, Score, Semester, TestDate, TestName, Timestamp, Type, Channel);

                string all_block_string = JsonConvert.SerializeObject(allblock);
                Console.WriteLine(all_block_string);

                Console.WriteLine("===========================");
                Console.WriteLine("Please type Yes, if you want add data again :)");
                string prompt = Console.ReadLine().ToLower();

                if (prompt == "yes")
                {
                    x = true;
                }
                else
                {
                    x = false;
                    break;
                }            
            }
        }
    }
}
