﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlockchainSourceCode
{
    class Certificates
    {
        public Byte[] content { get; set; }
        public string courseName { get; set; }
        public int credits { get; set; }
        public string keywords { get; set; }
        //public string mailaddr { get; set; }
        public string majorName { get; set; }
        public string name { get; set; }
        public string strata { get; set; }
        public string organization { get; set; }
        public double score { get; set; }
        public int semester { get; set; }
        public DateTime testDate { get; set; }
        public string testName { get; set; }
        public DateTime timestamp { get; set; }
        public string type { get; set; }
        public int channel { get; set; }
    }
}
