﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlockchainSourceCode
{
    class Block
    {
        public int Index { get; set; }
        public int Nonce { get; set; }
        public string Waktu { get; set; }
        public string PreviousHash { get; set; }
        public List<Transaction> Transactions { get; set; }
    }
}
